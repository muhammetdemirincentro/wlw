﻿using HtmlAgilityPack;
using System;
using System.Drawing;
using System.Net.Http;
using System.Threading.Tasks;
using Console = Colorful.Console;

namespace WelkLidWoord
{
    class Program
    {
        static void Main(string[] args)
        {
            var title = @"
_ _  _ ____ ____ _  _ ___ ____ ____    ___  _  _ ___ 
| |\ | |    |___ |\ |  |  |__/ |  |    |  \  \/   |  
| | \| |___ |___ | \|  |  |  \ |__|    |__/ _/\_  |  
                                                     
";
            if (args.Length == 0)
            {
                //Console.WriteLine("De applicatie wordt gebruikt doormiddel van de command 'ww' in de cmd venster in te voeren. Vervolgt met het woord waarvan je het lidwoord wilt weten");
                Console.BackgroundColor = Color.FromArgb(4, 28, 38);
                Console.Clear();
                Console.WriteLine(title, Color.FromArgb(255, 80, 0));
                OptionalMainLoop();
            }

            var lidWoord = GetLidWoord(args[0]);

            Console.WriteLine(String.Format("{0} {1}", lidWoord, args[0]));
            
        }

        public static string GetLidWoord(string woord="boom")
        {
            string uri = String.Format("https://www.welklidwoord.nl/{0}", woord);
           
            var web = new HtmlWeb();
            var doc = web.Load(uri);

            //gets the node in which the Lidwoord is stored
            var spanNodes = doc.GetElementbyId("content").SelectNodes("//span");

            var lidWoord = spanNodes[0].InnerText;
            return lidWoord;

        }

        public static void OptionalMainLoop()
        {
            Console.WriteLine("Stoppen met de applicatie kan doormiddel van CTRL+C");
            var quit = false;
            while (!quit)
            {
                Console.Write("Geef het woord waarvan je het lidwoord wilt weten: ");
                var woord = Console.ReadLine();

                var lidWoord = GetLidWoord(woord);

                Console.WriteLine(String.Format("{0} {1}", lidWoord, woord));
            }
        }
    }



}
